package controllers

import play.api._
import play.api.mvc._
import play.api.Play.current
import play.api.i18n.Messages.Implicits._

/**
 * Main application controller.
 */
class Application extends Controller {

  /**
   * Redirect to the product list.
   */
  def index = Action { implicit request =>
    Redirect(routes.Products.list())
  }
}
